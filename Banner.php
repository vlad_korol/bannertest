<?php
/**
 * Created by IntelliJ IDEA.
 * User: Vlad
 * Date: 16.07.2017
 * Time: 13:17
 */

namespace banner;

use mysqli;

/**
 * Class Banner
 * @package banner
 */
class Banner
{

    const DB_SERVER = "localhost";
    const DB_NAME = "ads";
    const DB_USER_NAME = "root";
    const DB_USER_PASS = "root";

    public function getImage($img = "mikasa.jpeg")
    {
        if (!file_exists($img)) return;

        self::saveToDb();

        header('content-type: image/gif');
        echo file_get_contents($img);
    }

    protected function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'] ?? "";
    }

    protected function getUserIp()
    {
        return $_SERVER['REMOTE_ADDR'] ?? "";
    }

    protected function getReferrer()
    {
        return $_SERVER['HTTP_REFERER'] ?? "";
    }

    protected function saveToDb()
    {
        $mysqli = new mysqli(self::DB_SERVER, self::DB_USER_NAME, self::DB_USER_PASS, self::DB_NAME);
        if ($mysqli->connect_errno) {
            // Mb save some info to log....
        }

        $sqlQuery = "INSERT INTO banners (ip_address, user_agent, page_url, views_count) VALUES (?,?,?,1) ON duplicate KEY UPDATE views_count = views_count + 1";

        if ($stmt = $mysqli->prepare($sqlQuery)) {
            $stmt->bind_param('iss', ip2long(self::getUserIp()), self::getUserAgent(), self::getReferrer());
            $stmt->execute();
        }
        $mysqli->close();
    }

}

$banner = new Banner();
$banner->getImage();